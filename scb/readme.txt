Code used to generate the web page https://www.somecrazyblogger.org/covid19/%CE%A9.html

Place these files in a single directory.
Execute in this order:

python3 update.py
python3 country_info.py confirmed log northern.png
python3 country_info.py confirmed log general.png
python3 country_info.py confirmed linear northern_linear.png
python3 country_info.py confirmed linear general_linear.png
python3 us_info.py confirmed log us.png
python3 us_info.py confirmed linear us_linear.png
python3 asian_info.py confirmed log asia.png
python3 asian_info.py confirmed linear asia_linear.png
python3 country_info.py confirmed log latin.png
python3 country_info.py confirmed linear latin_linear.png
python3 timestamp.py

